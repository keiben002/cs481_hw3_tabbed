﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        /*
         * This disappearing function changes the button color back to white 
         * and changes the label color back to white. At the same time rescaling the label
         * back down. What I like to call this function is a "reset" function because it remodels the
         * function to the way it was before the appearing function.
         */
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Artorias Page", "Next");

            Button button1 = color_changer;
           
            Label label = Artorias;

            button1.TextColor = Color.White;
            label.TextColor = Color.White;
            label.RelScaleTo(-4);
            Image image = art;
            image.RelScaleTo(-2);
        }

        /*
         * This on appearing event changes the color_changer button to color "MediumPurple"
         * and also re-scales the label "Artorias" by 2x and the picture by 2x 
         */
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
        
            Task.Delay(1200);
            Label label = Artorias;
            Button button = color_changer;
            button.TextColor = Color.MediumPurple;
            label.RelScaleTo(4);
            label.RelRotateTo(360, 1000);
            Image image = art;
            image.RelScaleTo(2);
        }

        //this function/button is for changing all the text colors to the color "Firebrick" upon pressing
        private void color_change(object sender, EventArgs e)
        {
            
            Label label = Artorias;
            Button button2 = color_changer;

            label.TextColor = Color.Firebrick;    //changes label(Artorius) text color to firebrick
            button2.TextColor = Color.Firebrick;

        }
        
    }
}


