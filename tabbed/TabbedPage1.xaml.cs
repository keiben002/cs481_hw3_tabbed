﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {


        /*
         * So here is the main centerpiece to my tabbed project. Basically, there is one main page that is my new TabbedPage
         * under this TabbedPage are it's children which allow it to have connected pages with tabs. This is different from
         * the hierachy because instead of using push and pop to get around, all we have to do is declare children(sub-pages)
         * which will act as navigation routes.
         */
        public TabbedPage1()
        {
            InitializeComponent();
            TabbedPage tabs = new TabbedPage();
            Children.Add(new Page1());
            Children.Add(new Page2());
            Children.Add(new Page3());
            Children.Add(new Page4());
        }
    }
}