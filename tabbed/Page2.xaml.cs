﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        /*Upon leaving the page the background of the page will change from DarkRed 
          to DarkGreen, and the Nameless King label that was changed from the font changer
          button will also be reset back to it's original name, fontsize, font attributes and color
        */
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            
            base.DisplayAlert("Notice!", "You have left Soul of Cinder Page", "Next");
            Task.Delay(1200);
            ContentPage content = page4_bckgrd;
            content.BackgroundColor = Color.Black;
            Label label = Nameless_King;
            label.FontSize = 24;
            label.Text = "Nameless King";
            label.TextColor = Color.Black;
            Button button = spoilers;
            button.Text = "**SPOILERS**";
            button.FontSize = 36;
        }

        //upon entering the page the background is changed from black to "Dark Red"
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(1200);
            ContentPage content = page4_bckgrd;
            content.BackgroundColor = Color.DarkRed;
            Label label = Nameless_King;
            label.Text = "Nameless King";
            await label.RelRotateTo(360, 1000);
        }

        /*When the button is pressed the label "Nameless King" will change to fontsize 36,
        background color Darkred, the text will change to "King of Storms", the color of the text will 
        change to "White" and the font will change to bold. Also the text "King of Storms" will be rotated 360 degrees.
        */
        private void Font_Changer(object sender, EventArgs e)
        {
            Label label = Nameless_King;
            label.FontSize = 36;
            label.BackgroundColor = Color.DarkRed;
            label.Text = "King of Storms";
            label.TextColor = Color.Gold;
            label.FontAttributes = FontAttributes.Bold;
            label.RelRotateTo(-360, 1000);
        }

        /*
         * This button function is literally acting as a spoiler button that you would see from reddit or any other similar site.
         * Basically, all it does is when you click on the button "**SPOILERS**" it will open a block text describing what the page
         * is all about. (You can find a similar function to this in my EXTRA page 4)
         */
        private void Spoiler(object sender, EventArgs e)
        {
            Button button = spoilers;
            button.FontSize = 18;
            button.Text = "The Nameless King was a god of war who wielded the power of dragons and lightning to battle his enemies.The Nameless King gets his name from a forgotten kingdom of which he ruled over many lifetimes ago. It is hinted that he is unknownly the first born child of Gywn, Lord of Sunlight";
        }
    }
}
