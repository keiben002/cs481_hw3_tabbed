﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
                        
        }
        /*
         * My Disappearing function displays an alert when left the page. 
         * When leaving the page it resets the background color to black. 
         * It resets the spoilers page from its block text to the button 
         * and resizes the picture back to its original size.
         */
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Sif's Page", "Next");
            ContentPage content = pg4_bg;
            content.BackgroundColor = Color.Black;
            Button button = spoilers;
            button.Text = "**SPOILERS**";
            button.FontSize = 36;
            Image image = sif1;
            image.RelScaleTo(-1);
        }

        /*
         * The onAppearing function for my EXTRA page 4 re-scales the picture by 1x its original size.
         * It then rotates the image 360 degrees within 1000 miliseconds. Although its difficult to see
         * it also changes the background coloring to gray from black
         */
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            Image image1 = sif1;
            image1.RelScaleTo(1);
            image1.RelRotateTo(360, 1000);
            ContentPage content2 = pg4_bg;
            content2.BackgroundColor = Color.Gray;
        
        }
        /*
         * This button function is literally acting as a spoiler button that you would see from reddit or any other similar site.
         * Basically, all it does is when you click on the button "**SPOILERS**" it will open a block text describing what the page
         * is all about.
         */
        private void Spoilers(object sender, EventArgs e)
        {
            Button button = spoilers;
            button.FontSize = 18;
            button.Text = "The Great Gray Wolf Sif was a legendary giant wolf who was partnered with Knight Artorias. Unfortunately when Artorias was swallowed by the abyss, Artorias gave Sif his shield to protect him against the dangers of the world. Now, Sif guards his lost friend's grave in attempt to make sure no one has to deal with what Artorias went through.";
        }
    }
}
