﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        /*When exiting the page, it will display an alert message that notifies the user that they have left the page.
        *It will also reset the text to the label back to white and bold for the next time the user enters the page.
        */
        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Notice!", "You have left Soul of Cinder Page", "Next");
            Label label = cinderlabel;
            label.TextColor = Color.White;
            label.FontAttributes = FontAttributes.Bold;
        }

        /*The label will fade out, change color to crimson, then fade back into view. After 2 seconds, it will
        *fade, change colors and then come back as a different color.
        */
        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(1200);
            Label label = cinderlabel;
           
            
            await label.FadeTo(-100, 1000);
            label.TextColor = Color.Crimson;
            await label.FadeTo(100, 1000);
            
            await label.FadeTo(-100, 2000);
            label.TextColor = Color.Aqua;
            await label.FadeTo(100, 2000);
            
        }

        /*This function controls the value of the slider. In this case the slider controls the opacity of the picture
          so when the value is increased on the slider the picture will become more visible. When the value is decreased
          the opacity of the picture is also decreased.
          
          Source: https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/slider

          **This is also the source that gave me many ideas on appearing and disappearing but I used my own thoughts and ideas
          for formulating what I wanted to accomplish my task**
       */
        private void Slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double val = e.NewValue;
            cinderimage.Opacity = val;
            
        }
    }
}